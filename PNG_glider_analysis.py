#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PNG_glider_analysis.py: analyses glider data from the Apr 19 glider deployment and 
compares with SHOC data

@author: danielboettger
"""

import sys, os, glob
import numpy as np
import pandas as pd
from natsort import natsorted

sys.path.append('..')
import ReadGliderData as RG
import ReadModelData as RM
import AnalyseData as AD

def main():

    # Load glider data
    glider = RG.ReadGlider(glider_file, load_path=glider_path)
    glider.CalculateDensity()
    glider.Save()
    gliderdata = AD.AnalyseData(glider.time['gridded'], glider.lat['gridded'], 
                       glider.lon['gridded'], glider.T['gridded'], 
                       glider.S['gridded'], glider.p['gridded'], 
                       glider.rho['gridded'], save_path=save_path,
                       data_type='glider_' + glider.mission)
    
    gliderdata.CalculateIsopycnalDispacement(sample_rate=1)
    gliderdata.CalculateSoundSpeedParameters()
    ballasting = gliderdata.CalculateBallasting()
    
    # Plot glider data
    gliderdata.PlotMap(gliderdata.displacement, 'Isopycnal Displacement (m)', 
                 'Isopycnal Displacement', z=20)
    gliderdata.PlotTimeSeries(gliderdata.isopycnals, 'Depth (m)', 'Depth', centre=False, 
                        vs='density', plot_SLD=False)
    gliderdata.PlotTimeSeries(gliderdata.displacement, 'Isopycnal Displacement (m)',
                        'Isopycnal Displacement', contour=True)
    gliderdata.PlotTimeSeries(gliderdata.T, 'Temperature (C)', 'Temperature', centre=False)
    gliderdata.PlotTimeSeries(gliderdata.S, 'Salinity (psu)', 'Salinity', centre=False)
    gliderdata.PlotTimeSeries(gliderdata.rho, 'Density (kg m^-^3)', 'Density', centre=False)
    gliderdata.PlotTimeSeries(np.abs(ballasting), 'Ballasting (tonnes/min)', 'Ballasting', 
                        contour=True, centre=False, levels=np.arange(0,.05,0.005))
    maxB = np.nanmax(ballasting, axis=0)
    gliderdata.PlotMap(maxB,  'Max Ballasting (tonnes/min)', 'Ballasting', centre=False, 
                 levels=np.arange(0,.1,0.005))

    # Add model data
    t_subset = pd.to_datetime(np.mean(glider.time['gridded'],axis=0), unit='D',
                              origin=glider.time_epoch)
    # Find runs that overlap with the glider deployment     
    files = natsorted(glob.glob(os.path.join(model_path,model_file)))
    print('Reading file:', files[0])
    shoc = RM.ReadModel(files[0], load_path=model_path, track=True, 
                            t=t_subset, z=glider.T['gridded'].index,
                            lat=glider.lat['gridded'], 
                            lon=glider.lon['gridded'])
    shoc.CalculateDensity()
    if '.nc' in model_path:
        shoc.Save()
#    T = np.ma.array(shoc.T, mask=np.isnan(shoc.T))
#    S = np.ma.array(shoc.S, mask=np.isnan(shoc.S))
#    rho = np.ma.array(shoc.rho, mask=np.isnan(shoc.rho))
    T = np.array(shoc.T.values, ndmin=3)
    S = np.array(shoc.S.values, ndmin=3)
    rho = np.array(shoc.rho.values, ndmin=3)
    
    return glider, gliderdata, shoc
    
    for i, file in enumerate(files[1:]):   
        print('Reading file:', file)
        shoc = RM.ReadModel(file, load_path=model_path, track=True, 
                            t=t_subset, z=glider.T['gridded'].index,
                            lat=glider.lat['gridded'], 
                            lon=glider.lon['gridded'])
        shoc.CalculateDensity()
        if '.nc' in model_path:
            shoc.Save()
        T = np.append(T, np.array(shoc.T.values, ndmin=3), axis=0) 
        S = np.append(S, np.array(shoc.S.values, ndmin=3), axis=0) 
        rho = np.append(rho, np.array(shoc.rho.values, ndmin=3), axis=0)  
        # Get a 'best estimate' time series, by generating an ensemble average  
#        T = np.average(np.ma.array([T, np.ma.array(shoc.T, mask=np.isnan(shoc.T))]),
#                       axis=0, weights=[i+1, 1])
#        S = np.average(np.ma.array([S, np.ma.array(shoc.S, mask=np.isnan(shoc.S))]),
#                       axis=0, weights=[i+1, 1])
#        rho = np.average(np.ma.array([rho, np.ma.array(shoc.rho, mask=np.isnan(shoc.rho))]),
#                       axis=0, weights=[i+1, 1])
       
    # Convert masked arrays back to a dataframe
#    shoc.T[:] = T
#    shoc.S[:] = S
#    shoc.rho[:] = rho
    # Convert back to a dataframe
    shoc.T[:] = np.nanmean(T, axis=0)
    shoc.S[:] = np.nanmean(S, axis=0)
    shoc.rho[:] = np.nanmean(rho, axis=0)

    shocdata = AD.AnalyseData(shoc.time, shoc.lat, shoc.lon, shoc.T, 
                       shoc.S, [], shoc.rho, save_path=save_path,
                       data_type='SHOC_ensemble',
                       time_epoch=shoc.time_epoch)
    shocdata.CalculateIsopycnalDispacement()
    ballasting = shocdata.CalculateBallasting(window=1)
    
    # Plot model data
    shocdata.PlotTimeSeries(shocdata.T, 'Temperature (C)', 'Temperature', centre=False)
    shocdata.PlotTimeSeries(shocdata.S, 'Salinity (psu)', 'Salinity', centre=False)
    shocdata.PlotTimeSeries(shocdata.displacement, 'Isopycnal Displacement (m)',
                        'Isopycnal Displacement', contour=True)
    shocdata.PlotMap(shocdata.displacement, 'Isopycnal Displacement (m)', 
                 'Isopycnal Displacement', z=20)
    shocdata.PlotTimeSeries(np.abs(ballasting), 'Ballasting (tonnes/min)', 'Ballasting', 
                        contour=True, centre=False, levels=np.arange(0,.005,0.0005))
    maxB = np.nanmax(ballasting, axis=0)
    shocdata.PlotMap(maxB,  'Max Ballasting (tonnes/min)', 'Ballasting', centre=False, 
                 levels=np.arange(0,.1,0.005))
    
    # Compare model to glider observations
    shocdata.PlotTimeSeries(shocdata.T - gliderdata.T,  'T(model) - T(glider) (C)', 'Tdiff', 
                     centre=True)
    
    return glider, gliderdata, shoc, shocdata
    

if __name__ == '__main__':
    
    glider_path = '/Users/danielboettger/Documents/Projects/gliders/data'
    glider_file = 'IMOS_ANFOG_BCEOPSTUV_20190422T120251Z_SL281_FV01_timeseries_END-20190514T210335Z.pkl'
#    model_path = '/Volumes/Boettger/NE_PNG/out_cf'
    model_path = '/Users/danielboettger/Documents/Projects/gliders/data/model'
    model_file = '*.pkl'
    save_path = os.path.join(glider_path,'figures')   
    
    glider, gliderdata, shoc = main()
