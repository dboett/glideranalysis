#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 08:56:06 2020

@author: danielboettger
"""

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import cm
from matplotlib.ticker import MultipleLocator
import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


def PlotMap(self, var, label, savename, **kwargs):
        """
        Make a plot of the glider track at a defined depth
        """
        z = kwargs.get('z',0)
        levels = kwargs.get('levels', 20)
        centre = kwargs.get('centre', True)
        cmap = 'RdBu' if centre else 'plasma'
                
        plt.figure(figsize=(10,10))
        ax = plt.axes(projection=ccrs.PlateCarree())
        
#        ax.set_extent((140,160,-10,-30))
        # Add some background data
        bc = cm.get_cmap('Blues', 4)
        ax.add_feature(cfeature.NaturalEarthFeature(category='physical', 
                                             name='bathymetry_L_0', 
                                             scale='10m',
                                             facecolor='None',
                                             edgecolor=bc(1/4)))
        ax.add_feature(cfeature.NaturalEarthFeature(category='physical', 
                                             name='bathymetry_K_200', 
                                             scale='10m',
                                             facecolor='None',
                                             edgecolor=bc(2/4)))
        ax.add_feature(cfeature.NaturalEarthFeature(category='physical', 
                                             name='bathymetry_J_1000', 
                                             scale='10m',
                                             facecolor='None',
                                             edgecolor=bc(3/4)))
        ax.add_feature(cfeature.NaturalEarthFeature(category='physical', 
                                             name='bathymetry_I_2000', 
                                             scale='10m',
                                             facecolor='None',
                                             edgecolor=bc(4/4)))
        
        ax.add_feature(cfeature.GSHHSFeature(edgecolor='k',
                                             facecolor='grey',
                                             scale='high'))
        
#        land = cfeature.NaturalEarthFeature('raster', 'cross-blend-hypso', '10m')
#        ax.add_feature(land)
        
#        fname = os.path.join(config["repo_data_dir"],
#                     'raster', 'natural_earth', '50-natural-earth-1-downsampled.png')
        
#        fname = '/Users/danielboettger/Downloads/HYP_HR/HYP_HR.tif'
#        from matplotlib.image import imread
#        img = imread(fname)
        
#        img = cartopy.io.RasterSource(fname, extent=[-180, 180, -90, 90])
#        
#        ax.imshow(img, origin='upper', extent=[-180, 180, -90, 90])
        
#        ax.add_raster(img)
        
#        from cartopy.io.img_tiles import Stamen
#        tiler = Stamen('terrain-background')
#        ax.add_image(tiler)
        
        cities = cfeature.NaturalEarthFeature('cultural', 'populated_places', '10m')
        ax.add_feature(cities)
        
        # Generate colormap
        l = len(levels) if hasattr(levels, '__len__') else levels
        cmap = cm.get_cmap(cmap, l)
        
        
        # Add data
        if centre:
            vmin = min(levels) if hasattr(levels, '__len__') else -np.nanmax(abs(var))
            vmax = max(levels) if hasattr(levels, '__len__')else np.nanmax(abs(var))
        else:
            vmin = min(levels) if hasattr(levels, '__len__') else np.nanmin(abs(var))
            vmax = max(levels) if hasattr(levels, '__len__') else np.nanmax(abs(var))
        norm = cm.colors.Normalize(vmax=vmax, vmin=vmin)
        if len(np.shape(var)) > 1:
            idx = np.argmin(abs(var.index - z))
            s = ax.scatter(self.lon, self.lat, c=var.iloc[idx,:],
                           zorder=np.inf, cmap=cmap, norm=norm)
        else:
            s = ax.scatter(self.lon, self.lat, c=var,
                           zorder=np.inf, cmap=cmap, norm=norm)

        # Label the first and last points
        mask = np.logical_and(np.isfinite(self.lon), 
                              np.isfinite(self.lat))
        t = self.time_epoch + datetime.timedelta(np.nanmin(self.time.min()))
        t = t.strftime('%d %b %y')       
        plt.text(self.lon[mask].iloc[0], self.lat[mask].iloc[0], t)
        t = self.time_epoch + datetime.timedelta(np.nanmax(self.time.max()))
        t = t.strftime('%d %b %y')      
        plt.text(self.lon[mask].iloc[-1], self.lat[mask].iloc[-1], t)

        gl = ax.gridlines(draw_labels=True)
        gl.xlines = False
        gl.ylines = False
        gl.xlabels_top = None
        gl.ylabels_right = None
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        
        # Add a colorbar
        cb = plt.colorbar(s, ax=ax, shrink=0.7, extend='both')
        cb.set_label(label)

        plt.tight_layout()

        self.SaveFigure(savename + '_' + 'map')
        
        plt.close()
        
        return
        
    
    def PlotTimeSeries(self, var, label, savename, **kwargs):
        
        vs = kwargs.get('vs', 'depth')
        centre = kwargs.get('centre', True)
        contour = kwargs.get('contour', True)
        log_scale = kwargs.get('log_scale', False)
        levels = kwargs.get('levels', 20)
        cmap = 'RdBu' if centre else 'plasma'
        plot_SLD = kwargs.get('plot_SLD', True)
        
        # Generate time axis for plotting
        if len(np.shape(self.time)) > 1:
            t = self.time.mean(axis=0)
            t = pd.to_datetime(t, unit='D', 
                           origin=self.time_epoch)
        else:
            t = pd.to_datetime(self.time, unit='D', 
                           origin=self.time_epoch)
        
        plt.figure(figsize=(10,5))
        ax = plt.axes()
        if log_scale:
            norm = matplotlib.colors.LogNorm(vmin=np.nanmin(var), vmax=np.nanmax(var))
        else:
            norm = matplotlib.colors.Normalize()
        if contour:
            p = plt.contourf(t, var.index, var.values, cmap=cmap, norm=norm,
                             extend='both', levels=levels)
        else:
            p = plt.pcolormesh(t, var.index, var.values, cmap=cmap, norm=norm, 
                               extend='both')
        
        if plot_SLD:
            if not hasattr(self, 'SLD'):
                self.CalculateSoundSpeedParameters()
            # Take a rolling mean over 10 profiles, just to smooth the data
            SLD_mean = self.SLD.rolling(10, center=True).mean().values                              
            ax.plot(t, SLD_mean*-1,'k', linewidth=0.5)
        
        if vs == 'depth':
            plt.ylabel('Depth (m)')
            plt.gca().invert_yaxis()
        elif vs == 'density':
            plt.ylabel('Density ($kg m^{-3}$)')
            plt.gca().invert_yaxis()
        if centre:
            plt.clim(-np.nanmax(abs(var.values)),
                     np.nanmax(abs(var.values)))
        ax.xaxis.set_major_locator(MultipleLocator(7))
        ax.xaxis.set_minor_locator(MultipleLocator(1))
        ax.grid(False)
        ax.grid(axis='x', which='major', alpha=0.75, linewidth=1)
        ax.grid(axis='x', which='minor', alpha=0.5, linewidth=0.5)
        # Add a colorbar
        cb = plt.colorbar(p, ax=ax)
        cb.set_label(label)
        
        plt.tight_layout()

        self.SaveFigure(savename + '_' + 'time')
        plt.close()
        return
        
    def SaveFigure(self,figname):
        
        plt.savefig(os.path.join(self.save_path, self.data_type +'_' + figname + '.png'))
        
        return