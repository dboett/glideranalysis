#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
AnalyseData.py: Analyses glider or model data and generates plots

@author: danielboettger
"""

import glidertools as gt
import xarray as xr

slocum = '/Volumes/Boettger/NE_PNG/glider_data/IMOS_ANFOG_BCEOPSTUV_20190422T120251Z_SL281_FV01_timeseries_END-20190514T210335Z.nc'
seaglider = '/Users/danielboettger/Downloads/IMOS_-_Australian_National_Facility_for_Ocean_Gliders_(ANFOG)_-_delayed_mode_glider_deployments_source_files 2/IMOS_ANFOG_BCEOPSTUV_20190422T120251Z_SL281_FV01_timeseries_END-20190514T210335Z.nc'

# gt.load.seaglider_show_variables(seaglider)
T = gt.load.seaglider.load_multiple_vars(seaglider, ['TEMP'])

# gt.plot.contourf('TIME', 'DEPTH', 'TEMP')

T['TIME'].TEMP.plot.line()

TT = T['TIME'].TEMP.values

x = T['TIME']
y = T['DEPTH']
ds = xr.open_dataset(seaglider)
profile = ds.variables['PROFILE']
Tgridded = gt.grid_data(profile, y, T['TEMP'])