#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
AnalyseData.py: Analyses glider or model data and generates plots

@author: danielboettger
"""
import os
import scipy.interpolate as scit
import gsw

import numpy as np
import pandas as pd
import xarray as xr
import traceback

import sys
sys.path.append('..')
import nc_config
sys.path.append('/Users/danielboettger/Documents/Projects/SoundSpeedPython')
import SoundSpeedParameters as SS

import time
import dask


def CalculateDensity(ds):
    """
    Wrapper for gsw.density.rho_t_exact.
    Calculates in-situ density of seawater from Absolute Salinity and 
    in-situ temperature. Note that the output, rho, is density, not density 
    anomaly; that is, 1000 kg/m^3 is not subracted from it.

    Parameters
    ----------
    ds : xarray DataSet
        DataSet containing variables T, S, p, with dimension t, z. If p is not
        provided, it is calculated with gsw.p_from_z

    Returns
    -------
    ds : xarray DataSet
        The original DataSet with the density field rho appended
    """
    
    if not hasattr(ds, 'p'):
        p = gsw.p_from_z(-1*np.abs(ds['z']), ds['lat'].mean()) # z must be negative
        ds['p'] = (ds['z'].dims, p)
        ds['p'] = ds['p'].broadcast_like(ds['T'])
        ds['p'].attrs = nc_config.assign_attributes('p')
    rho = gsw.density.rho_t_exact(ds['S'],ds['T'], ds['p'])
    ds['rho'] = (ds['T'].dims, rho)
    ds['rho'].attrs = nc_config.assign_attributes('rho')
        
    return ds


def CalculateIsopycnalDispacement(rho, z, t, window=2):  
         
    zz = z.broadcast_like(rho) if len(z.shape) == 1 else z
    tt = t.broadcast_like(rho) if len(t.shape) == 1 else t
    
    # Convert arrays to dataframes
    tt = tt.to_pandas()
    zz = zz.to_pandas()    
    
    zidx = zz.index.values
    tidx = tt.columns.values
    
    displacement = pd.DataFrame(index=zidx, columns=tidx, dtype='int64')
    
    if np.any(np.isfinite(rho)):
        try:
            # Calculate isopycnal depth
            minpyc = np.nanmin(rho)
            maxpyc = np.nanmax(rho)
            isopycnals = np.linspace(minpyc, maxpyc, len(zidx))
            
            isopyc_depth = pd.DataFrame(index=isopycnals,
                                      columns=tidx,
                                      dtype='int64')
            
            for i, it in enumerate(tidx):
                interpolant = scit.interp1d(rho[:,i].values,
                                            zz.iloc[:,i].values, 
                                            bounds_error=False, fill_value=np.nan)
                isopyc_depth.iloc[:,i] = interpolant.__call__(isopyc_depth.index)
                
            # Calculate mean isopycnal depth
            # Use a 3 day running mean (according to Boettger et al 2016)
            iso_mean = pd.DataFrame(index=isopycnals,
                                                columns=tidx,
                                                dtype='int64')
            for i, ipyc in enumerate(isopycnals):
                dt = np.mean(tt.iloc[i,:].diff().dt.seconds)
                # Calculate how many data points is required to achieve window size
                w = int(np.round(window*3600*24/dt))
                subset = pd.Series(isopyc_depth.iloc[i,:].values, index=tt.iloc[i,:])
                idx = np.nonzero((subset.notnull() & subset.index.notnull()).values)[0]
                iso_mean.iloc[i,idx] = subset.iloc[idx].rolling(w,center=True).mean().values
                    
            diff = isopyc_depth - iso_mean
            
            # Convert displacements from density to depth indexing
            
            for iprof in diff.columns:
                # plt.plot(isopyc_depth.loc[:,iprof].values,
                                            # diff.loc[:,iprof].values)   
                keep = np.isfinite(isopyc_depth.loc[:,iprof].values) & \
                            np.isfinite(diff.loc[:,iprof].values)
                if sum(keep) > 3:
                    interpolant = scit.interp1d(isopyc_depth.loc[:,iprof].values[keep],
                                                diff.loc[:,iprof].values[keep], 
                                                bounds_error=False, fill_value=np.nan)
                    displacement.loc[:,iprof] = interpolant.__call__(zz.loc[:,iprof])
                        
        except:
            traceback.print_exc()
   
    # Convert results to data arrays
    displacement = xr.DataArray(displacement, dims=rho.dims, coords=rho.coords,
                            name='rho_disp')
    displacement.attrs = nc_config.assign_attributes('rho_disp')
    displacement.attrs['comment'] = 'displacement of the isopycnal relative to a %i day mean' % window
    
    return displacement

 
def GlobalIsopycnalDisplacement(ds, xdim, ydim, window=2):
    """
    Calculate Isopycnal displacement at each lat, lon position in a dataset.
    Designed to process a model grid but could be applied in other ways.
    Uses Dask multiprocessing to calculate in parallel. 

    Parameters
    ----------
    ds : xarray DataSet
        DataSet containing variables T, S, rho with dimensions lat,lon, t, z. 
        If rho is not provided, it is calculated with CalculateDensity
    xdim : string
        Name of the x/longitude dimension in ds
    ydim : string
        Name of the y/latitude dimension in ds
    window : float, optional
        The size of the moving average window (in days) to be used in 
        CalculateIsopycnalDisplacement. The default is 2.

    Returns
    -------
    output : xarray DataSet
        The original dataset with the additional array iso_disp
    """

    # Check for presence of density
    if not hasattr(ds, 'rho'):
        ds = CalculateDensity(ds)
        
    output = []
    for i in ds[xdim].values:
        jj = []
        for j in ds[ydim].values:
            rho = ds.rho.sel({xdim:i,ydim:j})
            rho = rho.transpose(transpose_coords=True) 
            out = dask.delayed(CalculateIsopycnalDispacement)(rho, rho.z, rho.t)
            jj.append(out)
        output.append(jj)  
            
    output = list(dask.compute(*output, scheduler='processes'))
    
    output = xr.combine_nested(output, concat_dim=[xdim,ydim])
    output.transpose(*ds.rho.dims, transpose_coords=True)
    
    ds['isopyc_displacement'] = output
    
    return ds

    
def GlobalSoundSpeedParameters(ds, zdim):
    """
    Calculate sound speed paramaters at each lat, lon position, and for each time
    step in a dataset.
    Designed to process a model grid but could be applied in other ways.
    Uses ufunctions with Dask to calculate in parallel. 

    Parameters
    ----------
    ds : xarray DataSet
        DataSet containing variables T, S, p with dimensions lat,lon, t, z. 
        If p is not provided, it is calculated with gsw.p_from_z
    zdim : string
        Name of the z/depth dimension in ds

    Returns
    -------
    output : xarray DataSet
        The original dataset with the additional data arrays
    """
    
    if not hasattr(ds, 'p'):
        p = xr.apply_ufunc(gsw.p_from_z,
                           ds['z'],ds['lat'])
    else:
        p = ds['p']
    
    # output = xr.apply_ufunc(SS.CalculateDucts,
    #                         ds['T'], ds['S'], ds['z'], p, ds['lat'],
    #                         input_core_dims=[['k'],['k'],['k'],['k'],[]],
    #                         output_core_dims=[['k'],[],[],[],[],[],[]],
    #                         vectorize=True,
    #                         dask='parallelized')
    
    T_in_dims = [zdim] if zdim in ds['T'].dims else []
    S_in_dims = [zdim] if zdim in ds['S'].dims else []
    z_in_dims = [zdim] if zdim in ds['z'].dims else []
    p_in_dims = [zdim] if zdim in p.dims else []
    lat_in_dims = [zdim] if zdim in ds['lat'].dims else []
    
    output = xr.apply_ufunc(SS.CalculateDucts,
                            ds['T'], ds['S'], ds['z'], p, ds['lat'],
                            input_core_dims=[T_in_dims,S_in_dims,z_in_dims,
                                             p_in_dims,lat_in_dims],
                            output_core_dims=[[zdim],[],[],[],[],[],[]],
                            vectorize=True,
                            dask='parallelized')
    
    ds['c'] = (ds['T'].dims, output[0].transpose(*ds['T'].dims, transpose_coords=True))
    ds['c'].attrs = {'units':'m/s',
                  'long_name':'speed_of_sound',
                  'standard_name':'speed_of_sound_in_sea_water',
                  'valid_range': np.array([1000., 2000.]),
                  'comment':'calculated using the TEOS-10 toolbox: gsw.sound_speed_t_exact'}
    
    # Get order of dimensions for 2D variables
    d = list(ds['T'].dims)
    d.remove(zdim)
    d = tuple(d)
    ds['SLD'] = (d, output[1].transpose(*d, transpose_coords=True))
    ds['SLD'].attrs = nc_config.assign_attributes('SLD')
    ds['SLD_strength'] = (d, output[2].transpose(*d, transpose_coords=True))
    ds['SLD_strength'].attrs = nc_config.assign_attributes('SLD_strength')
    ds['SSC_axis'] = (d, output[3].transpose(*d, transpose_coords=True))
    ds['SSC_axis'].attrs = nc_config.assign_attributes('SSC_axis')
    ds['SSC_strength'] = (d, output[4].transpose(*d, transpose_coords=True))
    ds['SSC_strength'].attrs = nc_config.assign_attributes('SSC_strength')
    ds['DSC_axis'] = (d, output[5].transpose(*d, transpose_coords=True))
    ds['DSC_axis'].attrs = nc_config.assign_attributes('DSC_axis')
    ds['DSC_strength'] = (d, output[6].transpose(*d, transpose_coords=True))
    ds['DSC_strength'].attrs = nc_config.assign_attributes('DSC_strength')
    
    return ds


def CalculateBallasting(ds, displacement=3400, window=3, dim='t', t=None):
    """
    Calculate the rate of ballasting required by a submarine in order to maintain
    constant depth, taking into account density changes.
    Converts the density tendency into an equivalent mass of water (in tonnes)
    Units are tonnes per minute
    """
    if t is None:
        drho_dt = ds['rho'].differentiate(dim, datetime_unit='m')
    else:
        # If time is a variable and not a dimension, need to manually calculate
        # drho/dt
        axis = np.where(np.array(ds['rho'].dims) == dim)[0]        
        drho = np.gradient(ds['rho'], axis=axis)
        # Convert datatime to seconds
        epoch = np.datetime64('1970-01-01T00:00:00')
        tt = (ds[t] - epoch) / np.timedelta64(1, 'm')
        dt = np.gradient(tt, axis=axis)
        drho_dt = drho/dt
        drho_dt = xr.DataArray(drho_dt, dims=ds['rho'].dims)
        
    # Calculate volume of the submerged object
    volume = displacement/1025
    # db/dt is the rate of change of the ballast on the object required to offset 
    # the rate of change of the density
    db_dt = np.abs(volume * drho_dt)
    # Convert to tonnes / min
    db_dt = db_dt * 60
    
    # Calculate average time interval (in sec)
    if t is None:
        # Case when t is a dimensiondt is calculated nowand
        dt = pd.Series(ds[dim]).diff().dt.seconds.mean()
    else:
        # Case when t is a variable, and dt was calculated above
        dt = np.nanmean(dt)
        
    # Calculate how many data points is required to achieve window size
    w = int(np.round(window*60/dt))
    # Apply a rolling mean to filter some noise from the data
    ballasting = db_dt.rolling({ds[dim].dims[0]:w}, center=True, min_periods=1).mean()
    
    ds['ballasting'] = (ds['rho'].dims, ballasting)
    ds['ballasting'].attrs = nc_config.assign_attributes('ballasting')
        
    return ds
    

    
    
####################################################################################        
if __name__ == '__main__':
      
    # Test glider data (gridded)
    glider = xr.open_dataset('/Users/danielboettger/Documents/Projects/gliders/data/gridded.nc')  
    glider = CalculateDensity(glider)
    glider['isopyc_displacement'] = CalculateIsopycnalDispacement(glider['rho'], glider['z'],
                                                                  glider['t'])
    glider = GlobalSoundSpeedParameters(glider, 'z')
    glider = CalculateBallasting(glider,dim='profile', t='t')
    
    # glider.to_netcdf('/Users/danielboettger/Documents/Projects/gliders/data/gridded2.nc')
    
    
    sys.exit()
    
    # Test shoc data
    load_path = '/Volumes/Boettger/NE_PNG/out_cf/'
    infile = 'ne_png_2019050300.nc'
    outfile = 'subset.nc'
    
    names = nc_config.model_config('shoc')
    
    shoc = xr.open_dataset(os.path.join(load_path, infile))
    shoc = nc_config.rename_vars(shoc, names)
    
    # infile = 'ne_png_*.nc'
    # shoc = xr.open_mfdataset(os.path.join(load_path, infile), parallel=True)
    
    # shoc = CalculateDensity(shoc)
    
    # rho = shoc.rho.isel(i=100,j=100)
    # rho = rho.transpose(transpose_coords=True)
    
    # displacement, displacement_max = CalculateIsopycnalDispacement(rho, rho.z, rho.t)
    subset = shoc.sel(i=slice(100,200), j=slice(100,200))
    
    starttime = time.time()
    subset = CalculateDensity(subset)
    print('CalculateDensity: %0.3f minutes' %((time.time() - starttime)/60))
    
    # starttime = time.time()
    # subset = GlobalIsopycnalDisplacement(subset, 'i', 'j')
    # print('GlobalIsopycnalDisplacement: %0.3f minutes' %((time.time() - starttime)/60))
    
    # starttime = time.time()
    # subset = GlobalSoundSpeedParameters(subset, 'i', 'j', 'k', 'time')
    # print('GlobalSoundSpeedParameters: %0.3f minutes' %((time.time() - starttime)/60))
    
    # subset = xr.open_dataset(os.path.join(load_path, outfile))
    starttime = time.time()
    subset = CalculateBallasting(subset)
    print('CalculateBallasting: %0.3f minutes' %((time.time() - starttime)/60))
    
    subset.to_netcdf(os.path.join(load_path, 'subset2.nc'))
    # starttime = time.time()
    # output_multi = ModelGlobalIsopycnalDisplacement(subset, 'i', 'j')
    # print('Total time: {} minutes'.format((time.time() - starttime)/60))
    
    # subset['iso_disp'] = output.transpose('time', 'k', 'j', 'i', transpose_coords=True)
    
    # print(iso_out)
    
    # out = xr.apply_ufunc(CalculateIsopycnalDispacement,
    #                          subset.rho, subset.z, subset.t, 
    #                          input_core_dims=[['k','time'],['k'],['time']],
    #                          keep_attrs=True)