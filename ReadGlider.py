#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Lightweight object that stores 

@author: danielboettger
"""
import os
import glidertools as gt
import xarray as xr
import numpy as np
import gsw

import nc_config
       

def QcMask(glider, qc_vars, qc_flag=1):
    """
    Generate a QC mask using the inbuilt quality control flags.

    Parameters
    ----------
    glider : xr.Dataset
        The glider dataset
    qc_vars : list of strings
        Names of the qc variables to be used from the glider dataset
    qc_flag : int, optional
        Maximum flag number that is acceptable. The default is 1.

    Returns
    -------
    mask : bool array
        Boolean mask, True = good value, False = bad value.

    """  
    mask = xr.ones_like(glider[qc_vars[0]], dtype=bool)
    for ivar in qc_vars:
        mask = mask & (glider[ivar] <= qc_flag)
        
    return mask


def ProfileMask(z, profile_points, phase, profile_length=20, valid_phase=[1,4]):
    """
    Generate a mask that removes short profiles.
    
    Parameters
    ----------
    z : array, dtype=float, shape=[n,1]
        The depth values for the glider data
    profile_points : array, dtype=float, shape=[n,1]
        The profile variable from IMOS glider files. 
        The profile number is 1, and is increased at each phase change. It is 
        set to 0 when the glider is at surface (PHASE=0) or during inflexion 
        (PHASE=3) or if the profile is too short (less than 10 pressure 
        measurements)
    profile_length : float
        Length (in m) for what is considered a good profile. Profiles shorter than
        this are masked
    phase : list[int]
        Phases that are considered good. Phase of the trajectory at that time, 
        defined following EGO (see table 9 in the EGO user manual v1.1). 
        Values used: 0 for surface drifting, 1 for descending profile, 
        4 for ascending profile, 3 for inflexion. 

    Returns
    -------
    mask : bool array
        True = good values, False = bad values
        
    """
    # mask = xr.zeros_like(profile_points, dtype=bool)
    
    p = np.vstack([phase.values == i for i in valid_phase])
    phase_mask = np.any(p, axis=np.argmin(p.shape))
    
    mask = np.isfinite(profile_points) & (profile_points > 0) & phase_mask
    profiles = np.unique(profile_points[mask])
    
    for iprof in profiles:
        points = (profile_points == iprof)
        dz = np.nanmax(z[points]) - np.nanmin(z[points])
        if dz < profile_length:
            mask[points] = False
    
    return mask


def GridData(glider, gvars, x='profile', z='z', mask=None):
    """
    A wrapper around the glidertools.grid_data function. Grids glider data onto
    a uniform vertical grid and returns a new Dataset. 

    Parameters
    ----------
    glider : xarray Dataset
        The original glider data
    gvars : list of strings or str
        The variable/s in glider to be gridded
    x : str, optional
        The name of the horizontal coordinate to be used in gridding.
        The default is 'profile'.
    z : str, optional
        The name of the vertical coordinate to be used in gridding.
        The default is 'z'.        
    mask : array, dtype=bool, optional
        A boolean mask to discard values in the variable arrays. Values that are 
        False are not included in the binning. The default is None.

    Returns
    -------
    gridded : xarray Dataset
        A new Dataset containing the gridded arrays.

    """    
    # Get bins
    bins, median = gt.mapping.get_optimal_bins(glider['z'])
    
    # If only one gvar is passed, convert to list so that it works in the loop below
    if 'list'  not in str(type(gvars)):
        gvars = [gvars]
    
    # Empy list to collect gridded vars
    gridded = []
    
    for ivar in gvars:
        print('Gridding: %s' % ivar)
        print('dtype: %s' % str(glider[ivar].dtype))
        if 'datetime' in str(glider[ivar].dtype):
            # Bug in gt.grid_data means that we have to covert time to float
            print('Converting datetime64 to timestamp')
            epoch = np.datetime64('1970-01-01T00:00:00')
            var = (glider[ivar] - epoch) / np.timedelta64(1, 's')
        else:
            var = glider[ivar]

        if mask is not None:
            gridded.append(gt.grid_data(glider['profile'][mask], 
                                        glider['z'][mask], 
                                        var[mask],
                                        bins=bins))
        else:
            gridded.append(gt.grid_data(glider['profile'], 
                                        glider['z'], 
                                        var,
                                        bins=bins))
        # Change the default binned variable name back to the original var name
        gridded[-1] = gridded[-1].rename(ivar)
        if 'datetime' in str(glider[ivar].dtype):
            # Convert back to numpy datetime64
            print('Converting back to numpy datetime64')
            gridded[-1] = gridded[-1]*np.timedelta64(1, 's') + epoch
        
    gridded = xr.merge(gridded)
    
    # Copy global attributes to the new DataSet
    gridded.attrs = glider.attrs
    
    return gridded


def CalculateDensity(glider, T='T', S='S', p='p'):
        
    rho = gsw.density.rho_t_exact(glider[S], glider[T], glider[p])
           
    return rho

        

if __name__ == '__main__':
    
    load_path = '/Users/danielboettger/Documents/Projects/gliders/data'
    infile = 'IMOS_ANFOG_BCEOPSTUV_20190422T120251Z_SL281_FV01_timeseries_END-20190514T210335Z.nc'
    outfile = 'gridded.nc'
    
    names = nc_config.glider_config('imos')
    
    glider = xr.open_dataset(os.path.join(load_path, infile))
    glider = nc_config.rename_vars(glider, names)
    
    # Generate mask
    glider['qc_mask'] = QcMask(glider, ['T_qc', 'S_qc'])
    glider['profile_mask'] = ProfileMask(glider['z'], glider['profile'], glider['phase'])    
    mask = glider['qc_mask'] & glider['profile_mask']
    # Grid data
    gridded = GridData(glider, ['T', 'S', 'p', 't', 'lat', 'lon'], mask=mask)
    # Save to file
    gridded.to_netcdf(os.path.join(load_path, outfile))
    
    
        