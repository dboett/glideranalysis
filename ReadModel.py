#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ReadModel.py: Reads netcdf model data
Configured for SHOC only at this stage

@author: danielboettger
"""

import os
import scipy.interpolate as scit
import scipy.spatial as scsp
import xarray as xr
import numpy as np

import nc_config

# from matplotlib import pyplot as plt
    
        
def InterpolateToObs(model, variables, t, lat, lon, z=None, time_threshold=1,
                     dist_threshold=10): 
    """
    Interpolate model fields to observational data

    Parameters
    ----------
    model : xarray DataSet
        The model Dataset containing all required variables
    variables : list of strings
        The names of variables in model to be interpolated
    t : xarray DataArray or np array
        Observation times (length n)
    lat : xarray DataArray or np array
        Observation latitude (length n).
    lon : xarray DataArray or np array
        Observation longitude (length n).
    z : xarray DataArray or np array, optional
        Depth coordinates for the interpolated grid. If not provided, uses native
        model z coordinate
    time_threshold : float, optional
        The time difference between the observation and model time step in hours. 
        The default is 1.
    dist_threshold : float, optional
        The distance threshold between the observation and model positions in
        km. The default is 10.

    Returns
    -------
    interp : xarray DataSet
        A new DataSet containing the interpolated DataArrays and with the model
        attributes.

    """   
    # Assign z levels if not provided
    if z is None:
        z = model['z']
    
    # Create a new Dataset to hold the interpolated data
    interp = []
    
    distMatrix = scsp.distance_matrix(
                    np.transpose(np.array([np.ravel(model.lat),
                                           np.ravel(model.lon)])),
                    np.transpose(np.array([lat, lon], ndmin=2)),
                    p=2, threshold=100000)
    # Convert from degrees to km
    distMatrix = distMatrix * 60 * 1.85
    
    epoch = np.datetime64('1970-01-01T00:00:00')
    mt = np.transpose(np.array((model.t - epoch) / np.timedelta64(1, 'h'), ndmin=2))
    tt = np.transpose(np.array((t - epoch) / np.timedelta64(1, 'h'), ndmin=2))
    timeMatrix = scsp.distance_matrix(mt, tt, p=2, threshold=100000)
    it = np.argmin(timeMatrix, axis=0)
    dt = np.min(timeMatrix, axis=0)
    
    for ivar in variables:
        print('InterpolateToObs: %s' % ivar)
        data = xr.DataArray(np.ones((len(z),len(t)))*np.nan, 
                            dims=['z','t'], 
                            coords={'z':z.values, 't':t.values},
                            name=ivar, 
                            attrs=model[ivar].attrs)
        for i in range(len(t)):
            # Check within time threshold and distance threshold           
            if dt[i] < time_threshold and np.any(distMatrix[:,i] < dist_threshold):
                ilat, ilon = np.unravel_index(np.argmin(distMatrix[:,i]), np.shape(model.lat))
                # plt.plot(model.lon[ilat, ilon], model.lat[ilat,ilon],'.')
                # Interpolate onto the z levels
                keep = model[ivar][it[i],:,ilat,ilon].loc[model[ivar][it[i],:,ilat,ilon].notnull()]
                interpolant = scit.interp1d(keep.z, keep.values, 
                                    bounds_error=False, fill_value=np.nan)
                data[:,i] = interpolant.__call__(z)
                                
        interp.append(data)
        
    interp = xr.merge(interp)
    # Copy global attributes to the new DataSet
    interp.attrs = model.attrs
    # Add lat, lon, t as coordinates
    interp = interp.assign({'lat':lat, 'lon':lon})
    interp = interp.set_coords(['lat','lon'])
        
    return interp


if __name__ == '__main__':
    
    # Load saved glider data
    gridded = xr.open_dataset('/Users/danielboettger/Documents/Projects/gliders/data/gridded.nc')
    
    load_path = '/Volumes/Boettger/NE_PNG/out_cf/'
    infile = 'ne_png_2019050300.nc'
    outfile = 'gridded.nc'
    
    names = nc_config.model_config('shoc')
    
    shoc = xr.open_dataset(os.path.join(load_path, infile))
    shoc = nc_config.rename_vars(shoc, names)

    
    t = gridded.t.mean(dim='z')
    lat = xr.DataArray(gridded.lat.mean(dim='z').values, dims={'t'}, coords={'t':t.values})
    lon = xr.DataArray(gridded.lon.mean(dim='z').values, dims={'t'}, coords={'t':t.values})
        
    gridded['z'] = xr.DataArray(gridded['z'].values*-1, attrs=gridded['z'].attrs)
    gridded['z'].attrs['positive'] = 'up'
    vmin = gridded['z'].attrs['valid_max']*-1
    vmax = gridded['z'].attrs['valid_min']*-1
    gridded['z'].attrs['valid_min'] = vmin
    gridded['z'].attrs['valid_max'] = vmax    
    # z = xr.DataArray(np.arange(gridded.z.min(), gridded.z.max(), 1), name='z', attrs=gridded.z.attrs)
    z = gridded['z']
    
    shoc_slice = InterpolateToObs(shoc, ['T','S'], t, lat, lon, z)
    

    
    

    

