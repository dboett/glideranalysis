#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
nc_config.py - Maps variable names for commonly used netcdf files 

@author: danielboettger
"""

import warnings
import numpy as np

def glider_config(origin):
    """
    Parameters
    ----------
    format : TYPE
        DESCRIPTION.

    Returns
    -------
    names : dict of names

    """
    
    names = dict()
    
    if origin == 'imos':
        # Dimensions
        names['t'] = 'TIME'
        names['z'] = 'DEPTH'
        names['lat'] = 'LATITUDE'
        names['lon'] = 'LONGITUDE'
        # Variables
        names['T'] = 'TEMP'
        names['S'] = 'PSAL'
        names['p'] = 'PRES'
        names['profile'] = 'PROFILE'
        names['phase'] = 'PHASE'
        # QC variables
        names['T_qc'] = 'TEMP_quality_control'
        names['S_qc'] = 'PSAL_quality_control'
        names['p_qc'] = 'PRES_quality_control'
        
    names = dict([(value, key) for key, value in names.items()])
        
    return names

def model_config(origin):

    names = dict()
    
    if origin == 'shoc':
        # Dimensions
        names['t'] = 'time'
        names['z'] = 'zc'
        names['lat'] = 'latitude'
        names['lon'] = 'longitude'
        # Variables
        names['T'] = 'temp'
        names['S'] = 'salt'
        names['c'] = 'sound'
        names['u'] = 'u'
        names['v'] = 'v'
        
    elif 'adept' in origin:
        # Dimensions
        names['t'] = 'ocean_time'
        names['s'] = 's_rho'  # This is on sigma coordinates
        names['z'] = 'z_rho'  # This is on z coordinates
        names['lat'] = 'lat_rho'
        names['lon'] = 'lon_rho'
        # Variables
        # names['T'] = 'temp'
        # names['S'] = 'salt'
        names['c'] = 'soundspeed'
        names['u'] = 'u'
        names['v'] = 'v'
        
    elif origin == 'oceanmaps':
        # Dimensions
        names['t'] = 'Time'
        names['z'] = 'st_ocean'  # This is on sigma coordinates
        names['lat'] = 'yt_ocean'
        names['lon'] = 'xt_ocean'
        # Variables
        names['T'] = 'temp'
        names['S'] = 'salt'
        names['c'] = 'soundspeed'
        names['sld'] = 'max_depth'      
        names['u'] = 'u'
        names['v'] = 'v'

    names = dict([(value, key) for key, value in names.items()])
        
    return names


def rename_vars(dataset, names):
    
    # Check whether names are in datset
    bad_names = []
    for name in names.keys():
        if name not in dataset.variables.keys():
            warnings.warn('warning: variable %s not found' % name)
            bad_names.append(name)
    for name in bad_names:
        names.pop(name)
    
    # Rename variables that do exist
    dataset = dataset.rename_vars(names)

    return dataset


def assign_attributes(var):
    
    attrs = dict()
    
    attrs['SLD'] = {'units':'m',
                    'long_name':'sonic_layer_depth',
                    'valid_range': np.array([-1000., 0.]),
                    'positive':'up',
                    'comment':'Depth of the near-surface sound speed maximum'}

    attrs['SLD_strength'] = {'units':'m/s',
                             'long_name':'sonic_layer_strength',
                             'valid_range': np.array([0., 50.]),
                             'comment':'Difference between c(SLD) and c(surface)'}

    attrs['SSC_axis'] = {'units':'m',
                         'long_name':'shallow_sound_channel_axis',
                         'valid_range': np.array([-1000., 0.]),
                         'comment':'Depth of the shallow sound channel axis'}

    attrs['SSC_strength'] = {'units':'m/s',
                             'long_name':'shallow_sound_channel_strength',
                             'valid_range': np.array([0., 50.]),
                             'comment':'Difference between c(SSC_axis) and c(top/bottom of SSC)'}

    attrs['DSC_axis'] = {'units':'m',
                         'long_name':'deep_sound_channel_axis',
                         'valid_range': np.array([-3000., 0.]),
                         'comment':'Depth of the deep sound channel axis'}


    attrs['DSC_strength'] = {'units':'m/s',
                             'long_name':'deep_sound_channel_strength',
                             'valid_range': np.array([0., 100.]),
                             'comment':'Difference between c(DSC_axis) and c(top/bottom of DSC)'}
    attrs['c'] = {'units':'m/s',
                      'long_name':'speed_of_sound',
                      'standard_name':'speed_of_sound_in_sea_water',
                      'valid_range': np.array([1000., 2000.]),
                      'comment':'calculated using the TEOS-10 toolbox: gsw.sound_speed_t_exact'}
    
    attrs['rho_disp'] = {'units':'m',
                         'long_name':'isopyc_displacement',
                         'standard_name':'displacement_of_isopycnal',
                         'valid_range': np.array([-500., 500.]),
                         'positive':'up',
                         'comment':'displacement of the isopycnal relative to a xx day mean'} # Need to modify in place
    
    attrs['rho'] = {'units':'kg/m^3',
                    'long_name':'sea_water_density',
                    'standard_name':'sea_water_density',
                    'valid_range': np.array([1000., 1050.])}
    
    attrs['p'] = {'units':'dbar',
                  'long_name':'sea_water_pressure',
                  'standard_name':'sea_water_pressure',
                  'valid_range': np.array([-5., 5000.])}
    
    attrs['ballasting'] = {'units':'tonnes/min',
                    'long_name':'submarine_ballasting',
                    'valid_range': np.array([0., 100.]),
                    'comment':'absolute amount of ballasting required for a submarine to maintain depth. Filtered with a xx hour running mean'} # Need to modify in place}
    
    
    try:
        return attrs[var]
    except KeyError:
        print('Error: Attributes not defined for %s. Check nc_config.assign_attributes' %var)
        return
        
    
    
    
    