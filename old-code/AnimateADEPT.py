#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 13:22:17 2020

@author: danielboettger
"""

import os
from netCDF4 import Dataset
import scipy.interpolate as scit

import numpy as np
import pandas as pd
import datetime

import matplotlib
from matplotlib import pyplot as plt
import matplotlib.animation as animation
#plt.rcParams['animation.ffmpeg_path'] = '/usr/local/bin/ffmpeg'

from matplotlib.animation import FFMpegWriter
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

plt.style.use(os.path.join(os.path.dirname(__file__),'PlotStyles.mplstyle'))


def ReadADEPT(load_path, infile):
        
    file = Dataset(os.path.join(load_path, infile),'r')

    # Get dimensions
    time = file.variables['ocean_time'][:]
    time_units = file.variables['ocean_time'].units
    time_epoch = datetime.datetime.strptime(time_units, 
                                                     'seconds since %Y-%m-%d %H:%M:%S')        
    timestamp = pd.to_datetime(time, unit='s', 
                       origin=time_epoch)
    
    # ADEPT is on a sigma vertical coordinate system
    s_rho = np.transpose(np.array(file.variables['s_rho'][:], ndmin=3))
    stretch = file.variables['Cs_r'][:]
    h = np.array(file.variables['h'][:], ndmin=3)  # Depth of water
    z = np.ones((len(file.dimensions['s_rho']),
                         len(file.dimensions['eta_rho']),
                         len(file.dimensions['xi_rho'])))
    
    z = z*np.abs(s_rho*h) # Convert to positive down
    lat = file.variables['lat_rho']
    lon = file.variables['lon_rho']
               
    # Get typical variables
#        T = file.variables['temp']
#        S = file.variables['salt']
    c = file.variables['soundspeed']
    
    return timestamp, z, s_rho, stretch, lat, lon, c

def Animation(timestamp, z, s_rho, lat, lon, c, depth, transect, profile):
        
    transect_len = 500        
    max_depth = 500
    
    z_avg = np.nanmean(np.nanmean(z, axis=1), axis=1)
    max_idx = np.argmin(abs(max_depth-z_avg))
    z_idx = np.argmin(abs(depth-z_avg))
    
    t_lon = np.linspace(transect[0][0], transect[0][1],transect_len)
    t_lat = np.linspace(transect[1][0], transect[1][1],transect_len)
    t_s = np.linspace(s_rho.ravel()[max_idx],s_rho.ravel()[-1], 100)
    transect = []
    for iz in range(len(t_s)):
        for i in range(transect_len):
                transect.append([t_s[iz], t_lat[i], t_lon[i]])
    transect = np.array(transect)
                
    
    prof = []
    for iz in range(len(t_s)):
            prof.append([t_s[iz], profile[1], profile[0]])
    profile = np.array(prof)
    
    # Find color limits
    clim_m = [np.floor(np.nanmin(c[:,z_idx:,:,:])), np.ceil(np.nanmax(c[:,z_idx:,:,:]))]
#    clim_m[1] = 1540
    
    clim_t = clim_m
    
    # Set up figure    
    fig = plt.figure(figsize=(12,9))
    gs = matplotlib.gridspec.GridSpec(2, 2, width_ratios=[1, 5], height_ratios=[2,4]) 
    gs.tight_layout(fig)
    gs.update()

    # Main map plot
    axm = plt.subplot(gs[1,1], projection=ccrs.PlateCarree())
#    axm.set_extent([np.min(lon), np.max(lon), np.min(lat), np.max(lat)])
    axm.set_extent([np.min(lon), np.max(lon), 5, 23])
    gl = axm.gridlines(draw_labels=True)
    gl.xlines = False
    gl.ylines = False
    gl.xlabels_top = None
    gl.ylabels_right = None
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    
    # Map colorbar
    levels_m = np.arange(clim_m[0],clim_m[1],0.1)
    norm_m = matplotlib.colors.BoundaryNorm(levels_m, len(levels_m)-1)
    cmap_m = plt.cm.get_cmap('jet',len(levels_m))
    cb_m = plt.colorbar(matplotlib.cm.ScalarMappable(norm=norm_m, cmap=cmap_m),
                          extend='both')
    cb_m.set_label('c (m/s)')
    
    time_text = axm.text(0.02,0.98,'', 
                         transform=plt.gca().transAxes,
                         fontsize=10,fontweight='bold',
                         horizontalalignment='left',
                         verticalalignment='top',
                         bbox=dict(facecolor='white', alpha=0.5))
    
    # Basemap
    axm.add_feature(cfeature.GSHHSFeature(edgecolor='k',
                                             facecolor='grey',
                                             scale='high'))
#    cities = cfeature.NaturalEarthFeature('cultural', 'populated_places', '10m')
#    axm.add_feature(cities)
    
    # Transect
    axt = plt.subplot(gs[0,:])
    axt.set_xlabel('Longitude')
    axt.set_ylabel('$\sigma$ coord')

    
    # Transect colorbar
    levels_t = np.arange(clim_t[0],clim_t[1])
    norm_t = matplotlib.colors.BoundaryNorm(levels_t, len(levels_t)-1)
    cmap_t = plt.cm.get_cmap('jet',len(levels_t))
#    cb_t = plt.colorbar(matplotlib.cm.ScalarMappable(norm=norm_t, cmap=cmap_t),
#                          extend='both')
#    cb_t.set_label('c (m/s)')
    
    #Profile
    axp = plt.subplot(gs[1,0])
    axp.set_xlabel('c (m/s)')
    axp.set_ylabel('$\sigma$ coord')
    axp.set_xlim(clim_m)
    axp.set_ylim(t_s[0],t_s[-1])
    
    line_p, = axp.plot([],[])
    
    line_transect = axm.plot(transect[:,2], transect[:,1], 'k', transform=ccrs.Geodetic())
    
    point_profile = axm.plot(profile[0,2], profile[0,1], 'ro', transform=ccrs.Geodetic())
    
    def anim(it):
        
        # Map
        axm.collections = []  
        map_plot = axm.pcolormesh(lon, lat, c[it,z_idx,:,:], transform=ccrs.PlateCarree(), 
                       cmap=cmap_m, norm=norm_m)
        
        # Set up the interpolant for the transect
#        points = []
#        for (i, j, k), iz in np.ndenumerate(z):
#            points.append([iz, lat[j], lon[k]])
            
#        zz = np.array(z[z_idx:,:,:], ndmin=3)
#        iz,ilat,ilon = np.meshgrid(range(zz.shape[0]), range(zz.shape[1]), range(zz.shape[2]))
#        points = np.vstack([zz.ravel(), lat[ilat.ravel()], lon[ilon.ravel()]])
#        
#        iz,ilat,ilon = np.meshgrid(range(z.shape[0]), range(z.shape[1]), range(z.shape[2]))
#        points = np.vstack(z.ravel(), lat[ilat.ravel()], lon[ilon.ravel()])
#        interp = scit.NearestNDInterpolator(points, np.ravel(c))
        
        interp = scit.RegularGridInterpolator((s_rho.flatten()[max_idx:], lat, lon), c[it,max_idx:,:,:])
            
        
        # Transect
        c_transect = interp.__call__(transect)
        c_transect = np.reshape(c_transect,(len(t_s),len(t_lat)))
        
        axt.collections = []
        transect_plot = axt.pcolormesh(t_lon, t_s, c_transect,
                       cmap=cmap_t, norm=norm_t)
        
        # Profile
        c_profile = interp.__call__(profile)
        line_p.set_data(c_profile, t_s)
        
        time_text.set_text(timestamp[it])
        
        return map_plot, line_p, transect_plot, time_text
        
    
    a = animation.FuncAnimation(fig, anim, frames=c.shape[0],
                                    interval=200, blit=False, save_count=50)
                
    writer = FFMpegWriter(fps=5, bitrate=2500)
    
    a.save(os.path.join(load_path, 'animation.mp4'), writer=writer) 
    
    plt.close()
    
    
if __name__ == '__main__':
    
#    load_path = '/Users/danielboettger/Desktop/'
#    infile = 'sound.nc'
    
    load_path = os.get_cwd()
    infile = 'SCS_full.nc'
    
    timestamp, z, s_rho, stretch, lat, lon, c = ReadADEPT(load_path, infile)
    depth = 100
    transect = ([115, 125], [20, 20])
    profile = [118,20]
    Animation(timestamp, z, s_rho, lat, lon, c, depth, transect, profile)
    