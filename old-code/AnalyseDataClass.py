#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
AnalyseData.py: Analyses glider or model data and generates plots

@author: danielboettger
"""
import os
import scipy.interpolate as scit

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import cm
from matplotlib.ticker import MultipleLocator
import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

import numpy as np
import pandas as pd
import datetime

import sys
sys.path.append('..')
import ReadGliderData as RG
import ReadModelData as RM
plt.style.use(os.path.join(os.path.dirname(__file__),'PlotStyles.mplstyle'))
sys.path.append('/Users/danielboettger/Documents/Projects/SoundSpeedPython')
import SoundSpeedParameters as SS


class AnalyseData(object):
    
    def __init__(self, time, latitude, longitude, temperature, salinity, 
                 pressure, density, **kwargs):
        
        # Directory to save outputs to
        self.save_path = kwargs.get('save_path', os.getcwd())
        self.time_epoch = kwargs.get('time_epoch', datetime.datetime(1950, 1, 1, 0, 0))
        self.data_type = kwargs.get('data_type', '')
            
        # Get dimensions
        self.time = time
        self.lat = latitude
        self.lon = longitude
                
        # Get typical variables
        self.T = temperature
        self.S = salinity
        self.p = pressure
        self.rho = density

        return
              
    def CalculateIsopycnalDispacement(self, **kwargs):
        
        window = kwargs.get('window', 2) # Running mean window in days
        # Downsample (running mean) data in hours (use to match temporal 
        # resolution of glider to model) 
        sample_rate = kwargs.get('sample_rate', False) 
            
        assert (not isinstance(self.rho, bool)), \
            'To calculate insopycnal displacement, density needs to be provided'   
        
        # Calculate isopycnal depth
        minpyc = np.nanmin(self.rho)
        maxpyc = np.nanmax(self.rho)
        isopycnals = np.linspace(minpyc, maxpyc, len(self.rho.index))
        
        self.isopycnals = pd.DataFrame(index=isopycnals,
                                            columns=self.rho.columns,
                                            dtype='int64')
        
        for iprof in self.rho.columns:
            interpolant = scit.interp1d(self.rho.loc[:,iprof].values,
                                        self.rho.index, 
                                        bounds_error=False, fill_value=np.nan)
            self.isopycnals.loc[:,iprof] = interpolant.__call__(isopycnals)
            
        # Calculate mean isopycnal depth
        # Use a 3 day running mean (according to Boettger et al 2016)
        self.iso_mean = pd.DataFrame(index=isopycnals,
                                            columns=self.rho.columns,
                                            dtype='int64')
        self.iso_smooth = pd.DataFrame(index=isopycnals,
                                            columns=self.rho.columns,
                                            dtype='int64')
        for i, ipyc in enumerate(isopycnals):
            if len(np.shape(self.time)) > 1:
                t = pd.to_datetime(self.time.iloc[i,:].values, unit='D', 
                                   origin=self.time_epoch)
            else:
                t = pd.to_datetime(self.time.values, unit='D', 
                                   origin=self.time_epoch)
            dt = np.mean(t.to_series().diff().dt.seconds)
            # Calculate how many data points is required to achieve window size
            w = int(np.round(window*3600*24/dt))
            subset = pd.Series(self.isopycnals.iloc[i,:].values,
                               index=t)
            idx = np.nonzero((subset.notnull() & subset.index.notnull()).values)[0]
#            self.iso_mean.iloc[i,idx] = subset.iloc[idx].rolling('3D',
#                                 center=False, min_periods=200).mean().values
            self.iso_mean.iloc[i,idx] = subset.iloc[idx].rolling(w,
                                 center=True).mean().values
            
            # Downsampling using the sample rate
            if sample_rate:               
                w = int(np.round(sample_rate*3600*24/dt))
                idx = np.nonzero((subset.notnull() & subset.index.notnull()).values)[0]
                self.iso_smooth.iloc[i,idx] = subset.iloc[idx].rolling(w,
                                     center=True).mean().values 
        
        # Calculate isopycnal displacement
        if sample_rate:
            displacement = self.iso_smooth - self.iso_mean
        else:
            displacement = self.isopycnals - self.iso_mean
        
        # Convert displacements from density to depth indexing
        self.displacement = pd.DataFrame(index=self.rho.index,
                                            columns=self.rho.columns,
                                            dtype='int64')
        self.displacement_max = pd.Series(index=self.rho.columns,
                                            dtype='int64')
        for iprof in displacement.columns:
            interpolant = scit.interp1d(self.isopycnals.loc[:,iprof].values,
                                        displacement.loc[:,iprof].values, 
                                        bounds_error=False, fill_value=np.nan)
            self.displacement.loc[:,iprof] = interpolant.__call__(self.displacement.index)
            self.displacement_max.loc[iprof] = np.nanmax(self.displacement.loc[:,iprof])            
        
        return
    
    
    def CalculateSoundSpeedParameters(self):
        
        self.SLD = pd.Series(index=self.T.columns)
        self.c = pd.DataFrame(index=self.T.index,
                              columns=self.T.columns, 
                                                dtype='int64')
        
        for iprof in self.T.columns:
            c, SLD, _, _, _, _, _, = SS.CalculateDucts(self.T.loc[:,iprof],
                                                       self.S.loc[:,iprof],
                                                       self.T.index, [], 
                                                       self.lat.loc[iprof])
            
            self.c.loc[:,iprof] = c
            self.SLD.loc[iprof] = SLD
            
    def CalculateTendency(self, var):
        """
        Calculate the time rate of change of a variable in units of change per second
        """
        
        tendency = pd.DataFrame(index=var.index,
                              columns=var.columns, 
                                                dtype='int64')
        pad = np.zeros((np.shape(var)[0],1))
        if len(np.shape(self.time)) > 1:
            dt = np.diff(self.time, axis=1)
        else:
            dt = np.diff(self.time)
        tendency.loc[:] = np.append(pad, np.diff(var, axis=1) / dt, axis=1)
        # Time is in days, so convert tendency to changer per second
        tendency = tendency / (24 * 3600)
        
        return tendency
    
    def CalculateBallasting(self, **kwargs):
        """
        Calculate the rate of ballasting required by a submarine in order to maintain
        constant depth, taking into account density changes.
        Converts the density tendency into an equivalent mass of water (in tonnes)
        Units are tonnes per minute
        """
        displacement = kwargs.get('displacement', 3400)
        window = kwargs.get('window', 3) # Running mean window in hours
        
        tendency = self.CalculateTendency(self.rho)
        
        ballasting = pd.DataFrame(index=tendency.index,
                              columns=tendency.columns, 
                                                dtype='int64')
        
        # Calculating ballasting based on time rate of change of density
#        b = np.abs(displacement * tendency)
        b = np.abs((displacement/1025) * tendency)
        # Convert to tonnes / min
        b = b * 60
        # Low-pass filter with a moving average
        for i, iz in enumerate(ballasting.index):
            if len(np.shape(self.time)) > 1:
                t = pd.to_datetime(self.time.loc[iz,:].values, unit='D', 
                                   origin=self.time_epoch)
            else:
                t = pd.to_datetime(self.time.values, unit='D', 
                                   origin=self.time_epoch)
            # Calculate average time interval (in sec)
            dt = np.mean(t.to_series().diff().dt.seconds)
            # Calculate how many data points is required to achieve window size
            w = int(np.round(window*3600/dt))
            subset = pd.Series(b.loc[iz].values, index=t)
            idx = np.nonzero((subset.notnull() & subset.index.notnull()).values)[0]
            ballasting.iloc[i,idx] = subset.iloc[idx].rolling(w,
                                 center=True).mean().values 
        
        return ballasting
    

    def PlotMap(self, var, label, savename, **kwargs):
        """
        Make a plot of the glider track at a defined depth
        """
        z = kwargs.get('z',0)
        levels = kwargs.get('levels', 20)
        centre = kwargs.get('centre', True)
        cmap = 'RdBu' if centre else 'plasma'
                
        plt.figure(figsize=(10,10))
        ax = plt.axes(projection=ccrs.PlateCarree())
        
#        ax.set_extent((140,160,-10,-30))
        # Add some background data
        bc = cm.get_cmap('Blues', 4)
        ax.add_feature(cfeature.NaturalEarthFeature(category='physical', 
                                             name='bathymetry_L_0', 
                                             scale='10m',
                                             facecolor='None',
                                             edgecolor=bc(1/4)))
        ax.add_feature(cfeature.NaturalEarthFeature(category='physical', 
                                             name='bathymetry_K_200', 
                                             scale='10m',
                                             facecolor='None',
                                             edgecolor=bc(2/4)))
        ax.add_feature(cfeature.NaturalEarthFeature(category='physical', 
                                             name='bathymetry_J_1000', 
                                             scale='10m',
                                             facecolor='None',
                                             edgecolor=bc(3/4)))
        ax.add_feature(cfeature.NaturalEarthFeature(category='physical', 
                                             name='bathymetry_I_2000', 
                                             scale='10m',
                                             facecolor='None',
                                             edgecolor=bc(4/4)))
        
        ax.add_feature(cfeature.GSHHSFeature(edgecolor='k',
                                             facecolor='grey',
                                             scale='high'))
        
#        land = cfeature.NaturalEarthFeature('raster', 'cross-blend-hypso', '10m')
#        ax.add_feature(land)
        
#        fname = os.path.join(config["repo_data_dir"],
#                     'raster', 'natural_earth', '50-natural-earth-1-downsampled.png')
        
#        fname = '/Users/danielboettger/Downloads/HYP_HR/HYP_HR.tif'
#        from matplotlib.image import imread
#        img = imread(fname)
        
#        img = cartopy.io.RasterSource(fname, extent=[-180, 180, -90, 90])
#        
#        ax.imshow(img, origin='upper', extent=[-180, 180, -90, 90])
        
#        ax.add_raster(img)
        
#        from cartopy.io.img_tiles import Stamen
#        tiler = Stamen('terrain-background')
#        ax.add_image(tiler)
        
        cities = cfeature.NaturalEarthFeature('cultural', 'populated_places', '10m')
        ax.add_feature(cities)
        
        # Generate colormap
        l = len(levels) if hasattr(levels, '__len__') else levels
        cmap = cm.get_cmap(cmap, l)
        
        
        # Add data
        if centre:
            vmin = min(levels) if hasattr(levels, '__len__') else -np.nanmax(abs(var))
            vmax = max(levels) if hasattr(levels, '__len__')else np.nanmax(abs(var))
        else:
            vmin = min(levels) if hasattr(levels, '__len__') else np.nanmin(abs(var))
            vmax = max(levels) if hasattr(levels, '__len__') else np.nanmax(abs(var))
        norm = cm.colors.Normalize(vmax=vmax, vmin=vmin)
        if len(np.shape(var)) > 1:
            idx = np.argmin(abs(var.index - z))
            s = ax.scatter(self.lon, self.lat, c=var.iloc[idx,:],
                           zorder=np.inf, cmap=cmap, norm=norm)
        else:
            s = ax.scatter(self.lon, self.lat, c=var,
                           zorder=np.inf, cmap=cmap, norm=norm)

        # Label the first and last points
        mask = np.logical_and(np.isfinite(self.lon), 
                              np.isfinite(self.lat))
        t = self.time_epoch + datetime.timedelta(np.nanmin(self.time.min()))
        t = t.strftime('%d %b %y')       
        plt.text(self.lon[mask].iloc[0], self.lat[mask].iloc[0], t)
        t = self.time_epoch + datetime.timedelta(np.nanmax(self.time.max()))
        t = t.strftime('%d %b %y')      
        plt.text(self.lon[mask].iloc[-1], self.lat[mask].iloc[-1], t)

        gl = ax.gridlines(draw_labels=True)
        gl.xlines = False
        gl.ylines = False
        gl.xlabels_top = None
        gl.ylabels_right = None
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        
        # Add a colorbar
        cb = plt.colorbar(s, ax=ax, shrink=0.7, extend='both')
        cb.set_label(label)

        plt.tight_layout()

        self.SaveFigure(savename + '_' + 'map')
        
        plt.close()
        
        return
        
    
    def PlotTimeSeries(self, var, label, savename, **kwargs):
        
        vs = kwargs.get('vs', 'depth')
        centre = kwargs.get('centre', True)
        contour = kwargs.get('contour', True)
        log_scale = kwargs.get('log_scale', False)
        levels = kwargs.get('levels', 20)
        cmap = 'RdBu' if centre else 'plasma'
        plot_SLD = kwargs.get('plot_SLD', True)
        
        # Generate time axis for plotting
        if len(np.shape(self.time)) > 1:
            t = self.time.mean(axis=0)
            t = pd.to_datetime(t, unit='D', 
                           origin=self.time_epoch)
        else:
            t = pd.to_datetime(self.time, unit='D', 
                           origin=self.time_epoch)
        
        plt.figure(figsize=(10,5))
        ax = plt.axes()
        if log_scale:
            norm = matplotlib.colors.LogNorm(vmin=np.nanmin(var), vmax=np.nanmax(var))
        else:
            norm = matplotlib.colors.Normalize()
        if contour:
            p = plt.contourf(t, var.index, var.values, cmap=cmap, norm=norm,
                             extend='both', levels=levels)
        else:
            p = plt.pcolormesh(t, var.index, var.values, cmap=cmap, norm=norm, 
                               extend='both')
        
        if plot_SLD:
            if not hasattr(self, 'SLD'):
                self.CalculateSoundSpeedParameters()
            # Take a rolling mean over 10 profiles, just to smooth the data
            SLD_mean = self.SLD.rolling(10, center=True).mean().values                              
            ax.plot(t, SLD_mean*-1,'k', linewidth=0.5)
        
        if vs == 'depth':
            plt.ylabel('Depth (m)')
            plt.gca().invert_yaxis()
        elif vs == 'density':
            plt.ylabel('Density ($kg m^{-3}$)')
            plt.gca().invert_yaxis()
        if centre:
            plt.clim(-np.nanmax(abs(var.values)),
                     np.nanmax(abs(var.values)))
        ax.xaxis.set_major_locator(MultipleLocator(7))
        ax.xaxis.set_minor_locator(MultipleLocator(1))
        ax.grid(False)
        ax.grid(axis='x', which='major', alpha=0.75, linewidth=1)
        ax.grid(axis='x', which='minor', alpha=0.5, linewidth=0.5)
        # Add a colorbar
        cb = plt.colorbar(p, ax=ax)
        cb.set_label(label)
        
        plt.tight_layout()

        self.SaveFigure(savename + '_' + 'time')
        plt.close()
        return
        
    def SaveFigure(self,figname):
        
        plt.savefig(os.path.join(self.save_path, self.data_type +'_' + figname + '.png'))
        
        return
        
    
####################################################################################        
if __name__ == '__main__':
      
    load_path = '/Users/danielboettger/Documents/Projects/gliders/data'
    infile = 'IMOS_ANFOG_BCEOPSTUV_20190422T120251Z_SL281_FV01_timeseries_END-20190514T210335Z.nc'
#    
    glider = RG.ReadGlider(infile, load_path=load_path)
    glider.CalculateDensity()
    data = AnalyseData(glider.time['gridded'], glider.lat['gridded'], 
                       glider.lon['gridded'], glider.T['gridded'], 
                       glider.S['gridded'], glider.p['gridded'], 
                       glider.rho['gridded'], save_path=load_path,
                       data_type='glider_' + glider.mission)
    
