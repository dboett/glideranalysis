#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ReadGliderData.py: Reads netcdf glider data and returns arrays

@author: danielboettger
"""
import os
from netCDF4 import Dataset
import scipy.interpolate as scit
import gsw
import numpy as np
import pandas as pd
import datetime
import pickle


class ReadGlider(object):
    
    def __init__(self, infile, **kwargs):
        
        # Directory containing glider files
        self.load_path = kwargs.get('load_path', os.getcwd())
        # The netcdf file to read
        self.infile = infile 
        # Directory to save outputs to
        self.save_path = kwargs.get('save_path', self.load_path)
        
        # Open the file - either saved pickle or netcdf
        if '.nc' in infile:
            self.file = Dataset(os.path.join(self.load_path, self.infile),'r')
        
            # Get some details about the file
            try:
                # Get type of glider
                self.platform = self.file.variables['PLATFORM'].platform_type
                self.mission = self.file.deployment_code
            except:
                pass    
            
            # Get dimensions
            self.time = {'raw':self.file.variables['TIME'][:],
                      'gridded':pd.DataFrame()}
            
            self.time_units = self.file.variables['TIME'].units
            self.time_epoch = datetime.datetime.strptime(self.time_units, 
                                                         'days since %Y-%m-%d %H:%M:%S %Z')
            
    #        self.time['raw'] = pd.to_datetime(self.time['raw'], unit='D', origin=self.time_epoch)
            # Quasi-dimensions
            # Load z into memory to improve efficiency of code
            self.z = self.file.variables['DEPTH'][:]  # Gridded depth is built in to dataframes
            self.lat = {'raw':self.file.variables['LATITUDE'][:],
                      'gridded':pd.DataFrame()}
            self.lon = {'raw':self.file.variables['LONGITUDE'][:],
                      'gridded':pd.DataFrame()}
            
            # TODO: add BGC variables
            
            # Get typical variables
            self.T = {'raw':self.file.variables['TEMP'][:],
                      'qc':self.file.variables['TEMP_quality_control'][:], 
                      'gridded':pd.DataFrame()}
            self.S = {'raw':self.file.variables['PSAL'][:],
                      'qc':self.file.variables['PSAL_quality_control'][:], 
                      'gridded':pd.DataFrame()}
            self.p = {'raw':self.file.variables['PRES'][:],
                      'qc':self.file.variables['PRES_quality_control'][:], 
                      'gridded':pd.DataFrame()}
            
            # Profile numbers
            self.profile_points = self.file.variables['PROFILE'][:]
            self.profile_phase = self.file.variables['PHASE'][:]        
            
            # Initiate a blank qc mask
            self.qc_mask = np.ones_like(self.time, dtype=bool)*True
    
            # Close the netcdf file once done
    #        self.file.close()
        else:
            with open(os.path.join(self.load_path, self.infile), 'rb') as f:
                self.__dict__ = pickle.load(f).__dict__
        
        return
               
        
    def QcData(self, **kwargs):
        """
        Mask the arrays using the inbuilt quality control flags
        """
        
        qcflag = kwargs.get('qcflag', 1)  # Default: use only good data
        profile_length = kwargs.get('profile_length', 20) # Length (m) for what is considered a good profile
        
        z_qc = self.file.variables['DEPTH_quality_control']
                
        self.qc_mask = np.array((z_qc[:] == qcflag) & (self.T['qc'][:] == qcflag) \
                                & (self.S['qc'][:] == qcflag) \
                                & (self.p['qc'][:] == qcflag))
        
        valid = 100 * len(np.nonzero(self.qc_mask==True)[0]) / len(self.time['raw'])
        print(valid, '% of CTD values are valid')
        
        # Initialise a profile mask based on the qc field
        self.profile_mask = np.ones_like(self.profile_points, dtype=bool) * False
        
        bad = 0
        self.max_z = 0  # Maximum profile depth
        self.profiles = np.unique(self.profile_points)[1:]
        for iprof in self.profiles:
            points = np.nonzero((self.profile_points[:]==iprof) & \
                                ((self.profile_phase[:]==1) | \
                                 (self.profile_phase[:]==4)) & \
                                self.qc_mask)[0]
#            print(iprof, len(points))
            try:
                dz = abs(self.z[points[-1]] - self.z[points[0]])
                if dz < profile_length:
                    self.profiles = self.profiles[self.profiles != iprof]
                    bad += 1
                else:
                    self.profile_mask[points] = True
                    self.max_z = max(self.max_z, np.max(self.z[points]))
            except IndexError:
                bad += 1
                self.profiles = self.profiles[self.profiles != iprof]
                pass
    
        valid = 100 * (len(self.profiles) - bad) / len(self.profiles)
        print(valid, '% of profiles are longer than', profile_length, 'm')
        
        return 
    
    
    def __GridCoordinates__(self, z):
        """ 
        Create arrays of time, lat, lon to match the gridded variable
        arrays
        """
        
        self.time['gridded'] = pd.DataFrame(index=z, columns=self.profiles, dtype='int64')
        self.lat['gridded'] = pd.Series(index=self.profiles, dtype='int64')
        self.lon['gridded'] = pd.Series(index=self.profiles, dtype='int64')
        
        for ip, iprof in enumerate(self.profiles):
#            print(iprof)
            mask = np.nonzero((self.profile_points==iprof) & self.profile_mask & \
                                 self.qc_mask)[0]
            if len(mask) > 0:
                interpolant = scit.interp1d(self.z[mask], self.time['raw'][mask], 
                                            bounds_error=False, fill_value=np.nan)
                self.time['gridded'].loc[:,iprof] = interpolant.__call__(z)
                self.lat['gridded'].loc[iprof] = np.nanmean(self.lat['raw'][mask])
#                print(iprof)
#                print(self.lat['raw'][mask])
                self.lon['gridded'].loc[iprof] = np.nanmean(self.lon['raw'][mask])
            else:
                self.time['gridded'].drop(columns=iprof)
                self.lat['gridded'].drop(index=iprof)
                self.lon['gridded'].drop(columns=iprof)
            
        return
    
    
    def GridData(self, variable, **kwargs):
        """
        Grid profiles onto a uniform depth-profile grid
        """
        dz = kwargs.get('dz', 1)  # Depth steps (m)
        min_z = kwargs.get('min_z', 10)  # Minimum depth to keep
        qcflag = kwargs.get('qcflag', 1)  # Default: use only good data
        profile_length = kwargs.get('profile_length', 20) # Length (m) for what is considered a good
        
        if not hasattr(self, 'profile_mask'):
            self.QcData(qcflag=qcflag, profile_length=profile_length)
        
        z = np.arange(min_z, self.max_z, dz)
                
        data = pd.DataFrame(index=z, columns=self.profiles, dtype='int64')
        for ip, iprof in enumerate(self.profiles):
#            print(iprof)
            mask = np.nonzero((self.profile_points==iprof) & self.profile_mask & \
                                 self.qc_mask)[0]
            if len(mask) > 0:
                interpolant = scit.interp1d(self.z[mask], variable['raw'][mask], 
                                            bounds_error=False, fill_value=np.nan)
                data.loc[:,iprof] = interpolant.__call__(z)
            else:
                data.drop(columns=iprof)
        # Add to the dictionary for that variable
        variable['gridded'] = data
        
        if self.time['gridded'].empty:
            print('Generating gridded coordinates')
            self.__GridCoordinates__(z)
        
        return       
    
       
    def CalculateDensity(self, **kwargs):
        
        gridded = kwargs.get('gridded', True)  # Generate from gridded data
        raw = kwargs.get('raw', True)  # Generate from raw data
        
        self.rho = {'raw':'',
                    'gridded':''}
        
        if raw:
            # Calculate density
            self.rho['raw'] = gsw.density.rho_t_exact(self.S['raw'], self.T['raw'], 
                                          self.p['raw'])
        
        if gridded:
            if self.T['gridded'].empty:
                self.GridData(self.T)
            if self.S['gridded'].empty:
                self.GridData(self.S)
            if self.p['gridded'].empty:
                self.GridData(self.p)
            
            rho = gsw.density.rho_t_exact(self.S['gridded'], self.T['gridded'], 
                                          self.p['gridded'])
            
            self.rho['gridded'] = pd.DataFrame(index=self.T['gridded'].index,
                                                columns=self.T['gridded'].columns, 
                                                data=rho)
            
        return
    
    def Save(self, **kwargs):
        """
        Save the glider data in a pickle for later use
        """
        outfile = kwargs.get('outfile', 
                             os.path.join(self.load_path, 
                                          self.infile[0:self.infile.find('.')] + '.pkl'))

        self.file = '' # Need to remove links to netcdf file
        with open(outfile, 'wb') as f:
            pickle.dump(self,f)
            
        print('Saved data to:', outfile)
        
        return
        
    
####################################################################################        
if __name__ == '__main__':
    
  
    load_path = '/Users/danielboettger/Documents/Projects/gliders/data'
    infile = 'IMOS_ANFOG_BCEOPSTUV_20190422T120251Z_SL281_FV01_timeseries_END-20190514T210335Z.nc'
#    
    glider = ReadGlider(infile,load_path=load_path)
    glider.Save()
    glider2 = ReadGlider(infile[0:infile.find('.')] + '.pkl',load_path=load_path)
