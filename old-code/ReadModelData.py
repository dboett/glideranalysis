#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ReadModelData.py: Reads netcdf model data
Configured for SHOC only at this stage

@author: danielboettger
"""
import os
from netCDF4 import Dataset
import gsw
import scipy.spatial as scsp
import scipy.interpolate as scit
import pickle

import sys
sys.path.append('/Users/danielboettger/Documents/Projects/SoundSpeedPython')

import numpy as np
import pandas as pd
import datetime


class ReadModel(object):
    
    def __init__(self, infile, **kwargs):
        
        # Model type
        self.model = kwargs.get('model', 'SHOC')
        # Directory containing glider files
        self.load_path = kwargs.get('load_path', os.getcwd())
        # The netcdf file to read
        self.infile = infile 
        # Directory to save outputs to
        self.save_path = kwargs.get('save_path', self.load_path)
        # Only get a subset of the data
        t_subset = kwargs.get('t', False)
        lat_subset = kwargs.get('lat', False)
        lon_subset = kwargs.get('lon', False)
        # Z levels to interpolate on to 
        z_subset = kwargs.get('z', False)
        # Take a subset following a (glider) track. Requires t, lat and lon subsets
        self.track = kwargs.get('track', False)
        
        # Time and distance thresholds for use when following a track
        t_threshold = kwargs.get('t_threshold', 1/24)  # In days
        t_threshold = pd.to_timedelta(t_threshold, unit='D') # Convert to time delta
        dist_threshold = kwargs.get('dist_threshold', 5)  # In km
               
        
        # Open the file - either saved pickle or netcdf
        if '.nc' in self.infile:
            self.file = Dataset(os.path.join(self.load_path, self.infile),'r')        
        
            # Only configured for SHOC so far
            if self.model =='SHOC':
                self._ReadSHOC()  
            elif self.model == 'ADEPT':
                self._ReadADEPT()
                
            if np.any([t_subset, lat_subset, lon_subset]):
                print('Subsetting model data')
                # Check that t_subset is a Pandas Series
                if not isinstance(t_subset, pd.Series):
                    t_subset = pd.Series(t_subset)
                self.T = self.__Subset(self.T, t_subset, lat_subset, lon_subset, 
                                       z_subset, t_threshold, dist_threshold)
                self.S = self.__Subset(self.S, t_subset, lat_subset, lon_subset, 
                                       z_subset, t_threshold, dist_threshold)
                # Overwrite the dimensions with the subset arrays
                # (could instead overwrite with the corresponding dimensions from the model?)
                # Time is converted back to floats
                self.time = (t_subset - self.time_epoch)/pd.offsets.Day(1)
                self.timestamp = t_subset
                self.z = z_subset
                self.lat = lat_subset
                self.lon = lon_subset
            else:
                self.T = self.T[:]
                self.S = self.S[:]
            
            # SHOC gives nan vales as 1e35 - conver these to nan
#            self.T.iloc[self.T > 100] = np.nan
#            self.S.iloc[self.S > 100] = np.nan
        
        else:
            with open(os.path.join(self.load_path, self.infile), 'rb') as f:
                self.__dict__ = pickle.load(f).__dict__
        
        return
    
    def _ReadSHOC(self):
        
        # Get some details about the file
        try:
            # Get type of model run
            self.run_details = self.file.paramhead
        except:
            pass    
    
        # Get dimensions
        self.time = self.file.variables['time'][:]
        self.time_units = self.file.variables['time'].units
        try:
            self.time_epoch = datetime.datetime.strptime(self.time_units, 
                                                         'days since %Y-%m-%d %H:%M:%S +00:00')
        except:
            self.time_epoch = datetime.datetime.strptime(self.time_units, 
                                                         'days since %Y-%m-%d %H:%M:%S +00')
        self.timestamp = pd.to_datetime(self.time, unit='D', 
                           origin=self.time_epoch)
        
        self.z = np.abs(self.file.variables['zc'][:]) # Convert to positive down
        self.lat = self.file.variables['latitude']
        self.lon = self.file.variables['longitude']
                   
        # Get typical variables
        self.T = self.file.variables['temp']
        self.S = self.file.variables['salt']
        
        return
    
    
    def _ReadADEPT(self):
        
        # Get some details about the file
        try:
            # Get type of model run
            self.run_details = self.file.title
        except:
            pass    
    
        # Get dimensions
        self.time = self.file.variables['ocean_time'][:]
        self.time_units = self.file.variables['ocean_time'].units
        self.time_epoch = datetime.datetime.strptime(self.time_units, 
                                                         'seconds since %Y-%m-%d %H:%M:%S')        
        self.timestamp = pd.to_datetime(self.time, unit='s', 
                           origin=self.time_epoch)
        
        # ADEPT is on a sigma vertical coordinate system
        self.s_rho = np.transpose(np.array(self.file.variables['s_rho'][:], ndmin=3))
        self.h = np.array(self.file.variables['h'][:], ndmin=3)  # Depth of water
        self.z = np.ones((len(self.file.dimensions['s_rho']),
                             len(self.file.dimensions['eta_rho']),
                             len(self.file.dimensions['xi_rho'])))
        
        self.z = self.z*np.abs(self.s_rho*self.h) # Convert to positive down
        self.lat = self.file.variables['lat_rho']
        self.lon = self.file.variables['lon_rho']
                   
        # Get typical variables
#        self.T = self.file.variables['temp']
#        self.S = self.file.variables['salt']
        self.c = self.file.variables['soundspeed']
        self.T = self.file.variables['soundspeed']
        self.S = self.file.variables['soundspeed']
        
        return
                 
    
    def __Subset(self, variable, t_subset, lat_subset, lon_subset, z_subset, 
                 t_threshold, dist_threshold):
        """
        Take a subset of the data
        If only t_subset provided, return all lats and lons
        If only lat_subset and lon_subset, provide all times
        Need to provide both lat_subset and lon_subset, or neither
        """
        
        if np.any(t_subset == False) and not np.any([lat_subset, lon_subset]):
            # Returns only a subset of the times
            print('1')
            return
            
        if self.track:
            # Returns values along a (glider) track
            # Check that the subset arrays are the same size
            assert ((len(t_subset) == len(lat_subset)) and 
                    (len(t_subset) == len(lon_subset))), \
                    "To extract a track, all subset arrays must be the same length"
            # Check that t_subset is a pandas timestamp
            assert(t_subset.dtype == '<M8[ns]'), \
            'Time subset array must be a pandas timestamp'
            
#            from matplotlib import pyplot as plt
#            plt.figure()
#            plt.plot(lon_subset, lat_subset)
#            plt.plot(np.ravel(self.lon),np.ravel(self.lat),'.',c='grey', alpha=0.3)#                       
            
            # Generate dataframe            
            if np.any(z_subset == False):
                data = pd.DataFrame(index=self.z, dtype='int64')
            else:
                data = pd.DataFrame(index=z_subset, dtype='int64')
            # Generate a distance matrix
            distMatrix = scsp.distance_matrix(
                    np.transpose(np.array([np.ravel(self.lat),
                                           np.ravel(self.lon)])),
                    np.transpose(np.array([lat_subset, 
                                           lon_subset], ndmin=2)),
                    p=2, threshold=100000)
            # Convert from degrees to km
            distMatrix = distMatrix * 60 * 1.85
    
            for i, it in enumerate(t_subset.index):
                dt = np.min(abs(t_subset.loc[it] - self.timestamp))
                if dt < t_threshold and np.any(distMatrix[:,i] < dist_threshold):
                    itt = np.argmin(abs(t_subset.loc[it] - self.timestamp))
                    ilat, ilon = np.unravel_index(np.argmin(distMatrix[:,i]), np.shape(self.lat))
#                    plt.plot(self.lon[ilat, ilon], self.lat[ilat,ilon],'.')
                    if np.any(z_subset == False):
                        data.loc[:,it] = variable[itt,:,ilat,ilon]
                    else:
                        # Interpolate onto the z_subset levels
                        interpolant = scit.interp1d(self.z, variable[itt,:,ilat,ilon], 
                                            bounds_error=False, fill_value=np.nan)
                        data.loc[:,it] = interpolant.__call__(z_subset)
                else:
                    data.loc[:,it] = np.ones_like(data.index)*np.nan
        
        
        return data
    
       
    def CalculateDensity(self):
        
        p = gsw.p_from_z(-1*np.abs(self.z), np.nanmean(self.lat)) # z must be negative
        p = np.broadcast_to(np.transpose(np.array(p,ndmin=2)), np.shape(self.T))
        
        rho = gsw.density.rho_t_exact(self.S, self.T, p)
            
        self.rho = pd.DataFrame(index=self.T.index, columns=self.T.columns, 
                                                data=rho)
            
        return
          
    
    def Save(self, **kwargs):
        """
        Save the model data in a pickle for later use
        """
        outfile = kwargs.get('outfile', 
                             os.path.join(self.load_path, 
                                          self.infile[0:self.infile.find('.')] + '.pkl'))
        
        self.file = '' # Need to remove links to netcdf file
        with open(outfile, 'wb') as f:
            pickle.dump(self,f)
            
        print('Saved data to:', outfile)
        
        return
    
        
    
####################################################################################        
if __name__ == '__main__':
    
  
#    load_path = '/Volumes/Boettger/NE_PNG/out_cf'
#    infile = 'ne_png_2019041000.nc'
#    
#    t_subset = np.linspace(0,1,5)
#    t_subset = pd.to_datetime(t_subset, unit='D', 
#                               origin=pd.Timestamp(2019,4,29), box=False)
#    lat_subset = np.linspace(-1,0,5)
#    lon_subset = np.linspace(145,146,5)
#
#    shoc = ReadModel(infile, load_path=load_path, track=True, t=t_subset,
#                     lat=lat_subset, lon=lon_subset)
#    shoc.CalculateDensity()
#    shoc.Save()
#    shoc2 = ReadModel(infile[0:infile.find('.')] + '.pkl',load_path=load_path)

    load_path = '/Users/danielboettger/Desktop/'
    infile = 'sound.nc'
    
    adept = ReadModel(infile, load_path=load_path, model='ADEPT')
    
        

    
    

    

